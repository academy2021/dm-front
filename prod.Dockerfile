# build environment
FROM node:15.11.0-buster as builder

WORKDIR /app

COPY package*.json ./

RUN npm ci --silent --production

COPY . ./

RUN npm run build

# production environment
FROM nginx:1.19.8-alpine

COPY --from=builder /app/build /usr/share/nginx/html

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
