FROM node:15.11.0-buster

WORKDIR /app

COPY package*.json ./

RUN npm ci --silent

COPY . ./

EXPOSE 3000

ENTRYPOINT ["npm", "start"]
