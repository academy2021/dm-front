import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function factureReducer(state = initialState.facture, action) {
    switch (action.type) {
        case types.LOAD_FACTURE_SUCCESS:                      
            return action.facture;        
        default:
        return state;
    }
}