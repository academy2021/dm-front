import { combineReducers } from "redux";
import facture from "./factureReducer";
import cv from './cvReducer'

const rootReducer = combineReducers({
    facture,
    cv
});

export default rootReducer;