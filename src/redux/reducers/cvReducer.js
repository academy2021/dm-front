import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function factureReducer(state = initialState.cv, action) {
    switch (action.type) {
        case types.LOAD_CV_SUCCESS:                      
            return action.cv;        
        default:
        return state;
    }
}