import * as types from "./actionTypes";
import CvApi from "../../api/cvApi2";

export function loadCvSuccess (cv) {
    return { type: types.LOAD_CV_SUCCESS, cv }
}

export function loadCv(file) {
    return function(dispatch) {
        const cvApi = new CvApi();

        return cvApi.getCvSections(file)
        .then(result => {
            console.log('succes');
            dispatch(loadCvSuccess(result))
        })
        .catch(error => {
          alert('ko')
            throw error
        });
    }
}