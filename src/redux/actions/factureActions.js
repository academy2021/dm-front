import * as types from "./actionTypes";
import FactureApi from "../../api/factureApi2";

const factureApi = new FactureApi();

export function loadFactureSuccess (facture) {   
    return { type: types.LOAD_FACTURE_SUCCESS, facture }
}

export function loadFacture(file) {
    return function(dispatch) {
        return factureApi.getFactureSections(file)
        .then(model => {
            console.log(model);
            dispatch(loadFactureSuccess(model))
        })
        .catch(error => {
            console.log('-----------');
            console.log(error);
            throw error
        });
    };
}

export function loadNoteFrais(file) {
  return function(dispatch) { 
      return factureApi.getNoteFraisSections(file)
      .then(model => {
          console.log(model);
          dispatch(loadFactureSuccess(model))
      })
      .catch(error => {
          console.log('-----------');
          console.log(error);
          throw error
      });
  };
}