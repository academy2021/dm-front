import ApiService from "./apiService";
import { createFormData } from "../tools";

class FactureApi extends ApiService {
    constructor() {
        super(process.env.REACT_APP_API_FACTURE_URL);
    }

    getFactureSections(file) {
        return this.post("/api/extract", createFormData('facture', file));
    }

    getNoteFraisSections(file) {
        return this.post("/api/note/extract", createFormData('document', file));
    }
}

export default FactureApi;