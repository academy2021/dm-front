class ApiService {
    constructor(path) {
        this.path = path;
    }

    async handleResponse(response) {
        if(response.ok) return response.json();
        if(response.status === 400 || response.status === 401){
            const error = await response.text();
            throw new Error(error);
        }
        throw new Error("Network response was not ok!");
    }

    handleError(error) {
        console.error("API call failed :"+ error);
        throw error;
    }

    post(url, data) {
        const options = {
            method: "POST",
            body: data
        };

        return fetch(`${this.path}${url}`, options)
            .then(this.handleResponse)
            .catch(this.handleError);
    }
}

export default ApiService;