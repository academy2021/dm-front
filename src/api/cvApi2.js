import ApiService from "./apiService";
import { createFormData } from "../tools";

class CvApi extends ApiService {
    constructor() {
        super(process.env.REACT_APP_API_CV_URL);
    }

    getCvSections(file) {
        return this.post("/api/parsecv", createFormData('document', file));
    }
}

export default CvApi;