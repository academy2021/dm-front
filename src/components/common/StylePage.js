import { green, red, orange } from '@material-ui/core/colors';

const  useStyles = (theme) => {
    return {
        root: {
            minHeight: 225  
        },    
        section2: {
            margin: theme.spacing(2),
        },
        section1: {
            margin: theme.spacing(3, 2),
        },
        section3: {
            marginTop: theme.spacing(2),
        },
        center: {
            alignItems: 'center',
            justifyContent: 'center'
        },
        avatar:{
            fontSize: 14,        
        },
        inline: {
            display: 'inline',
        },
        paper:{
            height:230,       
            maxHeight:500, overflow:"scroll", overflowX: "hidden", overflowY:"auto"
        },
        top1:{
            color: green[500],
        },
        top2:{
            color: orange[500]
        },
        top3:{
            color: red['A700']
        }
    }
}

export default useStyles;