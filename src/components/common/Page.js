import { Component } from "react";
import PropTypes from "prop-types";
import { differenceInSeconds } from "date-fns";

class Page extends Component {

  state = {
    loading: false,
    files: [],
    open: false,
    time: null
  }

  getLoaderFunction() {
    throw new Error('You have to implement the method getLoaderFunction!');
  }

  handleSave(files) {
      const loaderFunction = this.getLoaderFunction();

      const startTime = Date.now()

      this.setState({loading: true, time: null})

      loaderFunction(files[0])
      .then(() => this.setState({time: differenceInSeconds(Date.now(), startTime)}))
      .catch(console.log)
      .finally(() => this.setState({loading: false}))

      this.setState({files: files, open: false});
  }

  handleChange(files){
    this.setState({files: files});
  }

  handleClose() {
    this.setState({open: false});
  }

  handleOpen() {
    this.setState({open: true});
  }
}

Page.propTypes = {
  facture: PropTypes.object,
  actions: PropTypes.object.isRequired
};

export default Page;
