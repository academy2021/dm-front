import React from "react";
import { NavLink } from "react-router-dom";

const Header = () => {

    const activeStyle = { color: "#F15B2A"}

    return (
        <nav>
            <NavLink to="/facture" activeStyle={activeStyle} exact>Facture</NavLink> { "| "}
            {/* <NavLink to="/noteFrais" activeStyle={activeStyle}>Note de Frais</NavLink> { "| "}  */}
            <NavLink to="/cv" activeStyle={activeStyle}>CV</NavLink> { "| "}      
            
        </nav>
    )
}

export default Header;