import { Route, Switch, BrowserRouter as Router } from "react-router-dom";
import { render, screen, fireEvent, findAllByRole } from '@testing-library/react';
import { Provider, connect } from 'react-redux';
import FacturePage from './FacturePage'
import configureStore from "./../../redux/configureStore";

const store = configureStore();
//jest.mock("FacturePage")

beforeEach(() => {
	render(
		<Provider store={store}>
			<Router>
				<FacturePage />
			</Router>
		</Provider>
	);
  });

test('renders learn react link', () => {
	expect(screen.getByText(/Facture Management/i)).toBeInTheDocument();
});

test('button to add document', () => {
	expect(screen.getByRole('button', {name: "Ajouter un Document"}))
});

test('button to add ticket de caisse', () => {
	expect(screen.getByRole('button', {name: "Ajouter Ticket de Caisse"}))
});

test('click de ajouter un doc', () => {
    expect(screen.queryByText(/Upload file/i)).not.toBeInTheDocument();
    fireEvent.click(screen.getByRole('button', {name: "Ajouter Ticket de Caisse"}))
    expect(screen.queryByText(/Upload file/i)).toBeInTheDocument();
})

