import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as factureActions from "../../redux/actions/factureActions";
import Card from '@material-ui/core/Card';
import { withStyles } from '@material-ui/core/styles';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import CssBaseline from '@material-ui/core/CssBaseline';
import Button from '@material-ui/core/Button';
import { DropzoneDialog } from 'material-ui-dropzone'
import Avatar from '@material-ui/core/Avatar';
import PostAdd from '@material-ui/icons/PostAdd'
import ListIcon from '@material-ui/icons/ArrowRight'
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Paper from '@material-ui/core/Paper';
import Switch from '@material-ui/core/Switch';
import ListSubheader from '@material-ui/core/ListSubheader';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ScheduleIcon from '@material-ui/icons/Schedule'
import CircularProgress from '@material-ui/core/CircularProgress';
import Chip from '@material-ui/core/Chip';
import useStyles from '../common/StylePage';
import { green } from '@material-ui/core/colors';

import Page from '../common/Page';

class FacturePage extends Page {

    state = {...super.state, isFacture: false}

    getLoaderFunction() {
        return this.state.isFacture? this.props.actions.loadFacture: this.props.actions.loadNoteFrais;
    }

    handleOpen() {
        this.setState({open: true, isFacture: true});
    }

    handleOpenNote() {
        this.setState({open: true, isFacture: false});
    }

render() {
    const { classes, facture } = this.props;
    return (
        <Card className={classes.root}>
            <CardHeader
                title="Facture Management"
                subheader="v0.1"
                avatar={
                    <Avatar aria-label="recipe" className={classes.avatar}>
                        <PostAdd/>
                    </Avatar>
                }
                action={
                    this.state.time &&(
                        <Chip size="small"  avatar={<Avatar><ScheduleIcon/></Avatar>} label= {"Temps d'éxecution: " + this.state.time + " second"} />
                )}
            />
            <CardContent>
                <Grid container spacing={2}>
                    <Grid container item xs={12} justify="center" alignItems="center">
                        <div style={{marginBottom:10}}>
                            <Button variant="outlined" onClick={this.handleOpen.bind(this)} style={{colors:green}}>
                                Ajouter un Document
                            </Button>
                            <Button variant="outlined" onClick={this.handleOpenNote.bind(this)} style={{marginLeft: 15}}>
                                Ajouter Ticket de Caisse
                            </Button>
                            <DropzoneDialog
                                open={this.state.open}
                                onSave={this.handleSave.bind(this)}
                                acceptedFiles={['image/jpeg', 'image/png', 'image/bmp']}
                                showPreviews={true}
                                maxFileSize={5000000}
                                onClose={this.handleClose.bind(this)}
                            />
                        </div>
                    </Grid>
                    {this.state.loading && (
                        <Grid container item xs={12} justify="center" alignItems="center">
                            <CircularProgress />
                        </Grid>
                    )}
                    {facture &&  !this.state.loading && (
                    <Grid container spacing={1} style={{ marginLeft:8,marginRight:8}}>
                        {Object.entries(facture).map((items, index) =>
                        {
                            const label=items[0].split(',')[0];
                            const score= (parseFloat(items[0].split(',')[1])* 100).toFixed(2)

                            let values=[]
                            let ispuce = false
                            if(label.startsWith('sectionTotal')) {
                                values = items[1].split('\n').filter(x=>x!=='')
                                ispuce=true
                            }else {
                                values.push(items[1])
                            }

                            return (
                                <Grid key={index} item xs={6} container direction="column" >

                                <Paper variant="outlined" className={classes.paper}>
                                    <List dense={true} className={classes.root}
                                        subheader={
                                            <Grid container spacing={2}>
                                                <Grid item xs={6} container direction="column">
                                                    <ListSubheader>{label}</ListSubheader>
                                                </Grid>
                                                <Grid item xs={6} container direction="column" justify="flex-end" alignItems="flex-end">
                                                    <ListSubheader>Score: {<span className={score > 90 ? classes.top1 : (score >70 ? classes.top2: classes.top3)}>{score + "%"}</span>}</ListSubheader>
                                                </Grid>
                                            </Grid>
                                        }
                                    >
                                        {values.map((item, index) =>  {
                                            return (
                                                <ListItem key={index}>
                                                    {ispuce && <ListItemIcon><ListIcon /></ListItemIcon>}
                                                    <ListItemText
                                                        style={{marginRight:20, paddingTop:1}}
                                                        id="switch-list-label-wifi"
                                                        primary={item} />
                                                    <ListItemSecondaryAction>
                                                        <Switch
                                                            edge="end"
                                                            inputProps={{ 'aria-labelledby': 'switch-list-label-wifi' }}
                                                        />
                                                    </ListItemSecondaryAction>
                                                </ListItem>
                                            )}
                                        )}
                                    </List>
                                </Paper>
                                {/* </Grid> */}
                                </Grid>
                            )
                        }
                    )}
                    </Grid>
                    )}
                </Grid>
            </CardContent>
        </Card>
    )
}


}


function mapStateToProps(state) {
    return {
        facture : state.facture,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions : {
            loadFacture: bindActionCreators (factureActions.loadFacture, dispatch),
            loadNoteFrais: bindActionCreators (factureActions.loadNoteFrais, dispatch),
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(useStyles)(FacturePage));
