import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as cvActions from "../../redux/actions/cvActions";
import Card from '@material-ui/core/Card';
import { withStyles } from '@material-ui/core/styles';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import Button from '@material-ui/core/Button';
import { DropzoneDialog } from 'material-ui-dropzone'
import Avatar from '@material-ui/core/Avatar';
import PostAdd from '@material-ui/icons/PostAdd'
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import useStyles from "../common/StylePage";
import SectionContainer from "./SectionContainer";

import Page from '../common/Page';

// ======================== CVPAGE ========================

class CvPage extends Page {

  state = {
    loading: false,
    files: [],
    open: false,
    time: null
  }

  getLoaderFunction() {
    return this.props.actions.loadCv;
  }

  render(){
    const { classes, cv } = this.props;
    const acceptedFilesTypes = [
      'image/jpeg',
      'image/png',
      'image/bmp',
      'application/pdf',
      'application/msword',
      'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    ];

    return (
      <Card className={classes.root}>

        <CardHeader
          title="CV Management" subheader="v0.1"
          avatar={
            <Avatar aria-label="recipe" className={classes.avatar}>
              <PostAdd/>
            </Avatar>
          }/>

          <CardContent>
            <Grid container spacing={2}>
              <Grid container item xs={12} justify="center" alignItems="center">
                <div style={{marginBottom:10}}>
                  <Button variant="outlined" onClick={this.handleOpen.bind(this)}>
                    Ajouter un Document
                  </Button>
                  <DropzoneDialog
                    open={this.state.open}
                    onSave={this.handleSave.bind(this)}
                    acceptedFiles={acceptedFilesTypes}
                    showPreviews={true}
                    maxFileSize={5000000}
                    onClose={this.handleClose.bind(this)}
                  />
                </div>
              </Grid>

              {this.state.loading && (
                <Grid container item xs={12} justify="center" alignItems="center">
                  <CircularProgress />
                </Grid>
              )}

              <Grid container spacing={1} style={{marginLeft:8, marginRight:8}}>
                {
                  cv && Object.entries(cv).map((items, index) =>
                      <SectionContainer items={items} key={index}/>
                  )
                }
              </Grid>
            </Grid>
          </CardContent>
      </Card>
    )
  }
}

function mapStateToProps(state) {
  return {
      cv : state.cv,
  };
}

function mapDispatchToProps(dispatch) {
  return {
      actions : {
        loadCv: bindActionCreators (cvActions.loadCv, dispatch)
      }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(useStyles)(CvPage));
