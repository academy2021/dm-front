
import React from "react";

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';

import SectionContent from './SectionContent';

const createSection = (content, section) =>
              <SectionContent values={content} index={section} classes />;
const createTypo1 = (content) => 
              <Typography variant="body2">{content}</Typography>;
const createTypo2 = (content) => 
              <Typography variant="body2" color="textSecondary" component="p">{content}</Typography>;

function chooseComponentFromSection(section, content) {
  // return a component depending on the section string
  const useTypo1 = ['Description', 'Profil'];
  const useTypo2 = ['Autres'];
  const useDisplaySection = ['Projets', 'Formations', 'Competences', 'Experiences',
                              'Infos_personnelles', 'Langues', 'Loisirs'];

  switch (true) {
    case useTypo1.includes(section):
      return createTypo1(content);

    case useTypo2.includes(section):
      return createTypo2(content);

    case useDisplaySection.includes(section):
      return createSection(content, section);
  }
}


const SectionContainer = ({items}) => {
  //const items = Object.entries(values);
  const [section, content] = items;

  return (
    <Grid item xs={12} key={section}>
      <Card variant="outlined">
        <CardHeader title={section}/>
        <CardContent style={{paddingTop:0}}>
          {chooseComponentFromSection(section, content)}
        </CardContent>
      </Card>
    </Grid>
  );
}

export default SectionContainer;
