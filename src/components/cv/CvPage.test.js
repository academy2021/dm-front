import React from 'react';
import { Provider } from 'react-redux';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import configureStore from 'redux-mock-store';
 
import CvPage from './CvPage';

const mockStore = configureStore([]);
const dummyCV = require('./dummyCV.json');

let store;

beforeEach(() => {
	store = mockStore({
	  cv: dummyCV
	});
});

test('CV Page : test if every keys from CV is displayed', async () => {
	render(
		<Provider store={store}>
			<CvPage/>
		</Provider>
	);

	for (let category of Object.keys(dummyCV)) {
		expect(screen.getByText(category)).toBeInTheDocument();
	}
})