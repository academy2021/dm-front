
import React from "react";

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListIcon from '@material-ui/icons/ArrowRight';

const SectionContent = ({values, index }) => {
  const items = Object.entries(values);

  return (
      <List key={index} dense={true} >
        {
          items.map((item, jdex) => {

            let [title, subTitles] = item;
            if (Array.isArray(subTitles)) {
              subTitles = subTitles.map(x=> x.trim()).join(', ');
            }

            return ( 
              <SectionContentBulletPoint key={jdex} title={title} text={subTitles} />
            );
          })
        }
      </List>
  );
}

const SectionContentBulletPoint = ({title, text}) => {
  return (
    <ListItem style={{paddingLeft:0}}>
      <ListItemIcon >
        <ListIcon style={{marginRight:0}}/>
      </ListItemIcon>

      <ListItemText 
        style={{paddingLeft:0 }} 
        id="switch-list-label-wifi" 
        primary={`${title} : ${text}`} />

    </ListItem>
  )
}

export default SectionContent;
