import { Route, Switch, BrowserRouter as Router } from "react-router-dom";
import { render, screen, fireEvent, findAllByRole } from '@testing-library/react';
import { Provider, connect } from 'react-redux';
import App from './App';
import configureStore from "./redux/configureStore";

const store = configureStore();

beforeEach(() => {
	render(
		<Provider store={store}>
			<Router>
				<App />
			</Router>
		</Provider>
	);
  });

test('renders learn react link', () => {
	expect(screen.getByText(/Facture Management/i)).toBeInTheDocument();
});

test('button to add doc', () => {
	expect(screen.getByRole('button', {name: "Ajouter un Document"}))
});

test('button to add ticket de caisse', () => {
	expect(screen.getByRole('button', {name: "Ajouter Ticket de Caisse"}))
});

test('button to go facture page', () => {
	expect(screen.getByRole('link', {name: "Facture"}))
});

test('button to go facture page', () => {
	expect(screen.getByRole('link', {name: "CV"}))
});