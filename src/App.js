//

import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Route, Switch } from "react-router-dom";
import { connect } from "react-redux";
import FacturePage from "./components/facture/FacturePage";
import CvPage from "./components/cv/CvPage";
import { makeStyles } from "@material-ui/core/styles";
import Header from "./components/common/Header";

const useStyles = makeStyles((theme) => ({
  progress: {
    alignItems: "center",
    justifyContent: "center",
  },
}));

function App() {
  const [loading, setLoading] = useState(false);

  const classes = useStyles();

  return (
    <div className="container-fluid">
      <Header />
      <Switch>
        <Route exact path="/" component={FacturePage} />
        <Route path="/facture" component={FacturePage} />
        <Route path="/cv" component={CvPage} />
      </Switch>
    </div>
  );
}

App.propTypes = {
  isConnected: PropTypes.func,
};

function mapStateToProps(state, ownProps) {
  //const { user } = state.auth;
  return {
    // auth: state.auth
  };
}

const mapDispatchToProps = {
  // isConnected
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
