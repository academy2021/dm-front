export function createFormData(key, file) {
    const formData = new FormData()
    formData.append(key, file)

    return formData;
}